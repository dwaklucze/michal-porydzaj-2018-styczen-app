import { expect } from 'chai';
import { CustomModule, collection } from './task.js';

describe("CustomModules", () => {

    let moduleChecker;

    beforeEach(() => {
        moduleChecker = new CustomModule({ collection });
    });

    it('should have collection', () => {
        expect(moduleChecker.modules).to.be.an('array')
    });

    it('should have object as item of collection', () => {
        moduleChecker.modules.filter((customModule) => {
            expect(customModule).to.be.an('object');
        });
    });

    it('should have name property', () => {
        moduleChecker.modules.filter((customModule) => {
            expect(customModule).to.have.property('name').that.is.a('string');
            expect(customModule.name).to.match(moduleChecker.moduleRe);
        });
    });

    it('should have module number', () => {
        expect(moduleChecker).to.have.property('moduleNumber').that.is.a('number')
        expect(moduleChecker._getCustomModuleNumber()).not.to.be.NaN;
    });

    it('should return max number', () => {
        expect(moduleChecker.moduleNumber).to.eql(11)
    });
});