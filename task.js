export class CustomModule {
    constructor(props) {
        this.moduleRe = new RegExp(props.regExp || '\\d+');
        this.modules = props.collection || [];
        this.moduleNumber = this._getCustomModuleNumber();
    }

    _getHighestNumber(collection) {
        return Math.max.apply(Math, collection);
    }

    _parseModulesToCollection() {

        if (!this.modules || !this.modules.length) {
            console.error('There are no modules!')
            return false;
        }

        return this.modules
            .map(module => this.moduleRe.exec(module.name)[0])
            .map(module => parseInt(module));
    }

    _getCustomModuleNumber() {
        const collection = this._parseModulesToCollection();
        return this._getHighestNumber(collection);
    }
}

export const collection = [
    { name: 'module 1' },
    { name: 'module name 2' },
    { name: 'module name 11' },
    { name: 'module 3' },
    { name: 'module 10' }
];

const testCustomModuleClass = new CustomModule({ collection });
console.log(testCustomModuleClass);
